/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'HARALD',
  tagline: 'Sikker adgangskontrol til dit datacenter',
  url: 'https://bennyn86.gitlab.io/harald/',
  baseUrl: '/harald/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/rfid.ico',
  organizationName: 'Harald', // Usually your GitHub org/user name.
  projectName: 'Datacenter FMS', // Usually your repo name.
  themeConfig: {  
    navbar: {
      title: 'HARALD',
      logo: {
        alt: 'My Site Logo',
        src: 'img/harald_logo.png',
      },
      items: [
        {
          label: 'Produktbeskrivelse',
          to: '/produktbeskrivelse',
          position: 'left',
        },
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Dokumentation',
          position: 'left',
        },
        {
          // href: 'https://gitlab.com/cam-does-code/fredericia_project',
          // label: 'GitLab',
          // position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Projektstyring',
          items: [
            {
              label: 'Use Case',
              to: '/use_case',
            },
            {
              label: 'Scrum',
              to: '/scrum',
            },
            // {
            //   label: 'Git',
            //   to: 'https://bennyn86.gitlab.io/harald/git',
            // },
          ],
        },
        {
          title: 'Om Os',
          items: [
            {
              label: 'Hvem er vi',
              to: '/gruppe4',
            },
          ],
        },
        {
          title: 'Links',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/cam-does-code/fredericia_project',
            },
            {
              label: 'Atlas Charts - Datacenter FMS',
              href: 'https://charts.mongodb.com/charts-datacenter-fms-qgwlk/public/dashboards/644f7063-deac-4d28-8aac-324712a95e91',
            },
          ],
        },
      ],
      // logo: {
      //   alt: 'Logo',
      //   src: 'img/harald_logo.png',
      //   width: 160,
      //   height: 51,
      // },
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
