---
title: Use Case
---

# Use Case

#### Use Case ID #0001: Adgangskontrol.   
- Primary Actors: Bruger   
- Secondary Actors:None   
- Short Description: Brugeren skal kunne låse døren op
- Pre-conditions: Systemet skal køre, kortet skal være et studiekort, brugeren skal eksistere i databasen.   
- Main-Flow:   
Brugeren scanner sit kort.   
Godkendt kort.   
Prompt ansigstsgenkendelse.   
Kamera startes automatisk.   
System sammenligner ansigt med billede i database.   
Godkendt, lås åbner.   
- Post-conditions:   
Låsen er åben.   
- Alternative flow:   
Error.   
Ugyldigt kort.   
Ansigt ikke genkendt.


![Organization](/img/use_case.png)