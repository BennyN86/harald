---
title: Vi er Gruppe 4
description: my hello page description
hide_table_of_contents: true
---

# Hvem er vi?

HARALD er udviklet af gruppe 4 på IT-Teknolog uddanelsen, på UCL Seebladsgade Odense.   
Projektet er udarbejdet i samarbejde med Datacenter uddannelsen på Fredericia Maskinmester Skole.  
Vores gruppe består af følgende personer:   
  
* Harald Brandt
* Christian Tverskov
* Mathias Kristensen
* Camilla Bartell
* Benny Nielsen

Her i vores fiktive selskab har vi indordnet os under følgende organisationsdiagram :smile:
![Organization](/img/org_chart.png)

HARALD skal etableres som et ApS selskab.  
Årsagen til dette er, at ApS-selskabet kun kræver en opstarts indskydelse på Kr. 40.000, hvor et A/S kræver et indskud på Kr. 400.000.   
En anden fordel ved ApS er, at det ikke kræver en bestyrelse.
