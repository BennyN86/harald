---
title: Produktbeskrivelse
---

# Hvad er HARALD

**HARALD** er et brugervenligt og intuitivt sikkerhedssystem, som beskytter mod uautoriseret adgang til datacentre.   
Ved at kombinere et personligt id-kort med ansigtsgenkendelse, sikrer systemet at brugeren matcher identiteten på id-kortet.   

![Organization](/img/harald.jpeg)