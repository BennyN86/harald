import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Brugervenligt',
    imageUrl: 'img/card_read.jpg',
    description: (
      <>
        HARALD er designet til at være et brugervenligt og intuitivt sikkerhedssystem for brugerne.
      </>
    ),
  },
  {
    title: 'Sikker adgang',
    imageUrl: 'img/secure.jpg',
    description: (
      <>
        Med ansigtsgenkendelsen tilføjes et ekstra lag af sikkerhed, og beskytter mod uautoriseret adgang.
      </>
    ),
  },
  {
    title: 'Brugbare data',
    imageUrl: 'img/data.jpg',
    description: (
      <>
        HARALD registrerer alle besøg i datacentret.   
        Alt data kan nemt tilgås og aflæses på en cloud-hostet platform.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
          <img
          src={require('/img/harald_logo.png').default}
            alt="Example banner"
          />
            {/* <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Get Started
            </Link> */}
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
