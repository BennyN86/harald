---
title: Scrum
---

# Scrum

Vi har benyttet scrum under projektet.

- Ugentlige stand-up møder med undervisere.
- Sprints varer 2 uger.
- GitLab: https://gitlab.com/cam-does-code/fredericia_project.
- Discord til koordination uden for arbejdstiden.
- Sprint Planning Meeting.
- Sprint Review Meeting.
- Sprint Retrospective Meeting.

![Organization](/img/kanban.png)

![Organization](/img/scrum.png)