module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Teknisk Dokumentation',
      items: [
        // 'getting-started',
        'kode',
        'add_user',
        'diagrammer',
        'komponentliste',
      ],
    },
  ],
};
