---
title: Python Kode
slug: /
---

### adgangskontrol.py - State Machine og Display
```py
from connect_db import user_lookup, ok
from reader import read
from main import camera, face_auth
from access import access_ok, access_denied
from RPi import GPIO
from time import sleep

import Adafruit_SSD1306
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

RST = 36
disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)
# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()
# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))
# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0


# Load default font.
#font = ImageFont.load_default()

# Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
font = ImageFont.truetype('Roboto-BoldCondensed.ttf', 27)

# Default state.
state = 1

try:
    while True:
        if state == 1: # Scanner kortet
            draw.rectangle((0,0,width,height), outline=0, fill=0)
            draw.text((x, top+24),       "Scan Kort", font=font, fill=255)
            disp.image(image)
            disp.display()
            print('state 1')
            read()
            state = 2
        if state == 2: # Kontrollere om det scannede kort findes i brugerdatabasen
            print('state 2')
            user_lookup()
            x = ok.get('access') 
            draw.rectangle((0,0,width,height), outline=0, fill=0)
            draw.text((x, top+0),  "Kontrollerer", font=font, fill=255)
            draw.text((x, top+30), "ID", font=font, fill=255)
            disp.image(image)
            disp.display()
            sleep(3)
            if x == 1: # Hvis den retunerede værdi er 1, findes ID-kortet i DB og ansigtskendelsen startes.
                state = 3
            elif x == 0: # Hvis værdien er = 0, findes ID-kortet ikke og brugeren afvises.
                state = 5
        if state == 3: # Starter kameraet og ansigtsgenkendelsen
            print('state 3')
            draw.rectangle((0,0,width,height), outline=0, fill=0)
            draw.text((x, top+0),  "Scanner", font=font, fill=255)
            draw.text((x, top+30),"Ansigt", font=font, fill=255)
            disp.image(image)
            disp.display()
            camera()
            auth = face_auth.get('auth')
            print(auth)
            if auth == 1: # Ansigt genkendt. 
                state = 4
            elif auth == 0: # Hvis det korrekte ansigt ikke er genkendt inden for 10 sekunder, nægtes adgangen.
                state = 5
        if state == 4: # Brugeren er genkendt og der gives adgang til datacenteret. Adgangen registreres i databasen.
            print('state 4')
            draw.rectangle((0,0,width,height), outline=0, fill=0)
            draw.text((x, top+24),       "Godkendt", font=font, fill=255)
            disp.image(image)
            disp.display()
            access_ok()
            state = 1
        if state == 5:# Brugeren blev ikke genkendt, adgang afvises.
            print('state 5')
            draw.rectangle((0,0,width,height), outline=0, fill=0)
            draw.text((x, top+24),       "Afvist", font=font, fill=255)
            disp.image(image)
            disp.display()
            access_denied()
            state = 1
except KeyboardInterrupt:
    print(' Stop!')
    draw.rectangle((0,0,width,height), outline=0, fill=0)
    draw.text((x, top+24),       "Annulleret", font=font, fill=255)
    disp.image(image)
    disp.display()
    GPIO.cleanup() 
```

### reader.py - koden aflæser id-kortets nummer.
```py
from RPi import GPIO
from mfrc522 import SimpleMFRC522

reader = SimpleMFRC522()
kort = {
    'id': 'null'
}

def read():
    print("Placer dit id kort på læseren")
    id, text = reader.read()
    print(id)
    print(text)
    kort['id'] = str(id)
```
### connect_db.py - Koden slår op i databasen og ser om brugeren findes.
```py
from pymongo import MongoClient
from reader import kort

monogodb_URL = "mongodb+srv://admin:admin@clusterfms.pbcxras.mongodb.net/?retryWrites=true&w=majority"
user = []
username = {
    'name': 'null'
}
ok = {
    'access': 0
}

def user_lookup():
    client = MongoClient(monogodb_URL)
    db = client.get_database("harald")
    col_users = db.get_collection('users')

    dblist = client.list_database_names()
    if "harald" in dblist:
        print("The database exists.")

    if col_users.find_one({"id": {"$eq": kort.get('id')}}):
        print("Bruger fundet")
        query = col_users.find_one({"id": kort.get('id')})
        user.append(query)
        username['name'] = query['fornavn']
        ok['access'] = 1
    else:
        print("Ukendt Bruger")
        ok['access'] = 0
```
### main.py - Kameraet udfører ansigtsgenkendelsen
```py
# Udeladt her, er de omkring 200 linjer kode til opsætning af kameraet 
        while True:
            for name, q in queues.items():
                # Add all msgs (color frames, object detections and face recognitions) to the Sync class.
                if q.has():
                    sync.add_msg(q.get(), name)

            msgs = sync.get_msgs()
            if msgs is not None:
                frame = msgs["color"].getCvFrame()
                dets = msgs["detection"].detections

                for i, detection in enumerate(dets):
                    bbox = frame_norm(frame, (detection.xmin, detection.ymin, detection.xmax, detection.ymax))
                    cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (10, 245, 10), 2)

                    features = np.array(msgs["recognition"][i].getFirstLayerFp16())
                    conf, name = facerec.new_recognition(features)
                    text.putText(frame, f"{name} {(100*conf):.0f}%", (bbox[0] + 10,bbox[1] + 35))
                    if name == username['name'] and 100*conf >= 80:
                        print('Fundet!')
                        face_auth['auth'] = 1
                        keyboard.send("q")
                        # Reset the start time
                        start_time = time.monotonic()           

                cv2.imshow("color", cv2.resize(frame, (800,800)))

            # Check if the time limit has been exceeded
                elapsed_time = time.monotonic() - start_time
                if elapsed_time > time_limit:
                    print('Timeout!')
                    face_auth['auth'] = 0
                    break

                if cv2.waitKey(1) == ord('q'):
                    break
```
### access.py - I denne kodeblok gives eller nægtes der adgang til datacentret.
```py
from pymongo import MongoClient
from time import sleep
from RPi import GPIO
from reader import kort
import datetime

monogodb_URL = "mongodb+srv://admin:admin@clusterfms.pbcxras.mongodb.net/?retryWrites=true&w=majority"

client = MongoClient(monogodb_URL)
db = client.get_database("harald")
collection = db.get_collection('access')

def access_ok():
    LED_Green = 18
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(LED_Green,GPIO.OUT)
    GPIO.output(LED_Green, GPIO.HIGH)
    print("Godkendt. Velkommen")
    data = {"id": kort,"timestamp":datetime.datetime.now()}
    x = collection.insert_one(data)
    print(x.inserted_id)
    sleep(5)
    GPIO.output(LED_Green, GPIO.LOW)
    GPIO.cleanup()


def access_denied():
    LED_Red = 16
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(LED_Red,GPIO.OUT)
    GPIO.output(LED_Red, GPIO.HIGH)
    print("Ukendt Kort! Ingen adgang!")
    sleep(5)
    GPIO.output(LED_Red, GPIO.LOW)
    GPIO.cleanup()                    

```


