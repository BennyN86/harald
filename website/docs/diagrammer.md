---
title: Diagrammer
---

I programmet fritzing har vi udarbejdet følgende diagrammer som dokumentation over de elektriske forbindelser i vores system.   

## Schematic
![Organization](/img/schematic.png)

## Breadboard View
![Organization](/img/breadboard.png)