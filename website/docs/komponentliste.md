---
title: Komponentliste
---

| Komponent                        | Antal | Link                                                                      |
|----------------------------------|-------|---------------------------------------------------------------------------|
| Raspberry Pi                     | 1     | https://www.raspberrypi.com/products/raspberry-pi-4-model-b/              |
| Raspberry Pi I/O Cable Extension | 1     | https://dk.rs-online.com/web/p/raspberry-pi-hats-og-add-ons/1812041       |
| Breadboard                       | 1     | https://minielektro.dk/breadboard-400-huller.html                         |
| Kamera                           | 1     | https://docs.luxonis.com/projects/hardware/en/latest/pages/BW1098OAK.html |
| RFID Sensor                      | 1     | http://www.handsontec.com/dataspecs/RC522.pdf                             |
| I2C OLED Display                 | 1     | https://arduinotech.dk/shop/0-96-inch-i2c-iic-spi-serial-128x64-oled/     |
| Rød LED                          | 1     | https://dk.rs-online.com/web/p/lysdioder/2285988                          |
| Grøn LED                         | 1     | https://dk.rs-online.com/web/p/lysdioder/2286004                          |
| Modstande 150 Ohm                | 2     | https://dk.rs-online.com/web/p/hulmonterede-modstande/4777732             |
| Modstande 10k Ohm                | 2     | https://dk.rs-online.com/web/p/hulmonterede-modstande/6832939             |
| 3D-Printet Produktkasse          | 1     |                                                                           |